<? include('header.php') ?>
<form class="container pt-5 page-msg">
  <div class="row">
    <div class="col-12 col-md-6 col-lg-4 mb-5">
      <!-- 群組列表篩選 -->

      <select name="" id="select-list" class="js-select2 form-control">
        <option value="">已完成</option>
        <option value="">未讀訊息</option>
        <option value="">垃圾訊息</option>
        <option value="">持續追蹤</option>
      </select>
    </div>

    <div class="col-12 col-md-6 col-lg-4 mb-5">
      <div class="form-inline">
        <div class="input-group rounded-0">
          <input type="text" class="form-control" placeholder="輸入名稱搜尋" name="search" value="" require>
          <div class="input-group-append">
            <button id="faqSearchBtn" class="btn btn-main" type="submit"><i class="fas fa-search"></i></button>
          </div>
        </div>
      </div>
    </div>

    <div class="col-12 col-md-6 col-lg-4 mb-5">
      <!-- <select name="" id="select-list">
      <option value="">已完成</option>
      <option value="">未讀訊息</option>
      <option value="">垃圾訊息</option>
      <option value="">持續追蹤</option>
      </select> -->
    </div>
  </div>

  <div class="row" style="max-height: 600px;">
    <!-- 群組列表 -->
    <div class="col-12 col-md-6 col-lg-4 h-100 mb-5">
      <div class="card h-100 overflow-auto" style="max-height: 600px;">
        <ul class="list-group">
        <? for ($i = 0; $i < 20; $i++) { ?>
          <li class="list-group-item d-flex justify-content-center align-items-center">
            <div class="mr-3 profile-img-box">
              <img src="assets/img/demo-news.png" alt="" class="msg-user-img">
            </div>

            <div class="profile-content-box">
              <div class="d-flex justify-content-between">
                <!-- 最大字數限制 20 (同 line) -->
                <h4 class="d-inline-block title-list">Show FishShow FishShow Fis Show Fish 123123</h4>
                <!-- <span class="d-inline-block text-sm text-muted"><i class="fab fa-line"></i>{聊天室名稱}</span> -->
              </div>
              <div class="d-flex justify-content-between">
                <span class="d-inline-block text-sm text-muted text-truncated line-clamp-1 mr-1">{你}:測試推波測試推波測試推波測試推波測試推波測試推波測試推波測試推波測試推波測試推波</span>
                <span class="d-inline-block text-sm text-muted">
                  <!-- <i class="fab fa-line"></i> -->
                  11:13
                </span>
              </div>
            </div>
          </li>

        <? } ?>
        </ul>

      </div>
    </div>
    <div class="col-12 col-md-6 col-lg-8 h-100 mb-5">
      <div class="card h-100 overflow-auto" style="max-height: 600px;">
      <!-- 聊天內容 -->
        <div class="card-header">
          <h5 class="card-title cursor-pointer hover-color-main" data-toggle="modal" data-target="#exampleModal">Show Fish</h5>
        </div>

        <div class="card-body overflow-auto">
          <div class="msg-time text-muted">
            昨天 1:11
          </div>
          <div class="msg-block left">
              <div class="msg-block-img">
                <img src="assets/img/demo-news.png" alt="" class="msg-user-img">
              </div>
              <div class="msg-block-content">
                <div class="msg-content">
                <div class="msg-text">
                  <a href="https://dashboard.chatisfy.com/">https://dashboard.chatisfy.com/</a>
                </div>
                  <div class="msg-text-time">1:11</div>
                </div>

                <div class="msg-content">
                <div class="msg-text">
                  <a href="https://dashboard.chatisfy.com/">https://dashboard.chatisfy.com/</a>
                  Lorem ipsum dolor sit, amet consectetur adipisicing elit. Ipsa, non ratione labore aliquid maiores doloribus obcaecati neque deserunt dicta nulla dolorem, fugiat nisi rem illum ipsam. Amet perspiciatis eligendi id!
                </div>
                  <div class="msg-text-time">1:13</div>
                </div>
              </div>
          </div>

          <div class="msg-time text-muted">
            今天 11:11
          </div>
          <div class="msg-block right">
              <div class="msg-block-content">
                <div class="msg-content">
                  <div class="msg-text">
                    <a href="https://dashboard.chatisfy.com/">https://dashboard.chatisfy.com/</a>
                  </div>
                  <div class="msg-text-time">11:11</div>
                </div>

                <div class="msg-content">
                  <div class="msg-text">
                    <a href="https://dashboard.chatisfy.com/">https://dashboard.chatisfy.com/</a>
                    Lorem ipsum dolor sit,ametconsecteturadipisicingelit. Ipsa,nonrationelaborealiquidmaioresdoloribusobcaecati neque deserunt dicta nulla dolorem, fugiat nisi rem illum ipsam. Amet perspiciatis eligendi id!
                  </div>
                  <div class="msg-text-time">11:13</div>
                </div>
              </div>
              <div class="msg-block-img">
                <img src="assets/img/demo-news.png" alt="" class="msg-user-img">
              </div>
          </div>
        </div>
        <div class="card-body">
        <!-- <div class="form-inline"> -->
          <div class="input-group rounded-0">
            <input type="text" class="form-control" placeholder="Type Message" name="message" value="" require>
            <div class="input-group-append">

              <button class="btn btn-main" type="submit"><i class="fas fa-paper-plane"></i></button>
            </div>
          </div>
        </div>
      </div>
    </div>

    <!-- Modal -->
    <div class="modal fade" id="exampleModal" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
      <div class="modal-dialog" role="document">
        <div class="modal-content">
          <div class="modal-header pb-0 border-0">
            <button type="button" class="close" data-dismiss="modal" aria-label="Close">
              <span aria-hidden="true">&times;</span>
            </button>
          </div>
          <div class="modal-body d-flex justify-content-center align-items-center pt-0">
            <div class="profile-img-box with-img-big mr-3">
              <img src="assets/img/demo-news.png" alt="" class="msg-user-img big mb-2">
              <br>
              <a href="" class="btn btn-main btn-sm">檢視更多</a>
            </div>

            <div class="profile-content-box with-img-big">
              <div>
                <h4 class="d-inline-block title-list">Show Fish</h4>
              </div>
              <div class=" text-sm text-muted">
                <div>
                  <span class="d-inline-block info-title mr-2">最近互動</span>
                  <span class="d-inline-block text-sm text-muted info-content">2020-10-20 19:50</span>
                </div>
                <div>
                  <span class="d-inline-block info-title mr-2">綁定帳號</span>
                  <span class="d-inline-block text-sm text-muted info-content">account.name</span>
                </div>
                <div>
                  <span class="d-inline-block info-title mr-2">訂閱狀態</span>
                  <span class="d-inline-block text-sm text-muted info-content">訂閱中</span>
                </div>
                <div>
                  <span class="d-inline-block info-title mr-2">手機號碼</span>
                  <span class="d-inline-block text-sm text-muted info-content">0900-000000</span>
                </div>
              </div>
            </div>
          </div>
          <!-- <div class="modal-footer">
            <button type="button" class="btn btn-cyan" data-dismiss="modal">Close</button>
            <button type="button" class="btn btn-primary">Save changes</button>
          </div> -->
        </div>
      </div>
    </div>
  </div>
</form>

<?include('footer.php')?>

<script>
  $(document).ready(function (e) {
    // JQuery select2
    $('.js-select2').select2({
      width: '100%',
      theme: 'default',
      minimumResultsForSearch: Infinity
    })

    $('.msg-text').on('click', function (e) {
      $this = $(this)
      $thisTime = $this.siblings('.msg-text-time')

      $('.msg-text-time').not($thisTime).hide()
      $thisTime.slideToggle()
    })
  })
</script>