<? include('header.php') ?>
<?
// 表單 input 編號
// $data = array(
//   'idxMap' => array(
//     '0' => array (
//       'lastIndex' => '1',
//       'type' => 'text'
//     ),
//     '1' => array (
//       'lastIndex' => '2',
//       'type' => 'multi-page'
//     ),
//   )
// )
?>
<form class="page-dashboard container pt-5">
  <div class="row">
    <div class="col-12">
      <div class="text-right">
        <div class="btn btn-main"><i class="fas fa-save mr-2"></i>送出</div>
        <div class="btn btn-main"><i class="fas fa-reply mr-2"></i>返回</div>
      </div>
    </div>
    <div class="col-12 col-md-6">
      <div class="form-group mb-3">
        <label for="">名稱</label>
        <input type="text" class="form-control">
      </div>
    </div>

    <div class="col-12 col-md-6">
      <div class="form-group mb-3">
        <label for="">狀態</label>
        <select name="" id="" class="form-control">
          <option value="">啟用</option>
          <option value="">停用</option>
        </select>
      </div>
    </div>
  </div>

  <div class="row mb-3">
    <div class="col-md-6">
      <div class="dropdown dropright">
        <a class="btn btn-main dropdown-toggle" href="#" role="button" id="dropdownMenu_0" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
        <i class="fa fa-plus"></i> 新增訊息
        </a>

        <ul class="dropdown-menu" aria-labelledby="dropdownMenu_0">
          <li class="dropdown-item hover-color-main js-btn-add-card" data-type='text'><i class="fa fa-plus"></i> &nbsp; 文字</a>
          <li class="dropdown-item hover-color-main js-btn-add-card" data-type='image'><i class="fa fa-plus"></i> &nbsp; 圖片</a>
          <li class="dropdown-item hover-color-main js-btn-add-card" data-type='video'><i class="fa fa-plus"></i> &nbsp; 影片</a>
          <li class="dropdown-item hover-color-main js-btn-add-card" data-type='image-text'><i class="fa fa-plus"></i> &nbsp; 圖文訊息</a>
          <li class="dropdown-item hover-color-main js-btn-add-card" data-type='multi-page'><i class="fa fa-plus"></i> &nbsp; 多頁訊息</a>
        </div>
      </div>
    </div>
  </div>

  <div class="row mb-3">
    <div class="col-12 text-danger" id="err_list_exceed_max_len"></div>
  </div>


  <div class="row">
  <div class="col-4">
    <div  id="tabList_wrap" class="list-group" id="list-tab" role="tablist">
      <? $key = 0 ?>
      <a class="js-sort-li list-group-item list-group-item-action" id="nav_<?=$key?>" data-toggle="list" href="#tab_cont_<?=$key?>" role="tab" aria-controls="nav_<?=$key?>" draggable="true">
        <i class="fas fa-arrows-alt handle"></i>
        <span class="js-num"></span>
        文字訊息
        <div class="js-title"></div>
      </a>

      <? $key = 1 ?>
      <a class="js-sort-li list-group-item list-group-item-action" id="nav_<?=$key?>" data-toggle="list" href="#tab_cont_<?=$key?>" role="tab" aria-controls="nav_<?=$key?>" draggable="true">
        <i class="fas fa-arrows-alt handle"></i>
        <span class="js-num"></span>
        多頁訊息
        <div class="js-title"></div>
      </a>
    </div>
  </div>
  <div class="col-8">
    <div class="tab-content" id="tabContent_wrap">
      <? $key = 0 ?>
      <div class="tab-pane fade active show" id="tab_cont_<?=$key?>" role="tabpanel" data-type="text" aria-labelledby="nav_<?=$key?>">
        <div class="js-card">
          <!-- 卡片 -->
          <div class="card mb-5">
            <div class="card-header">
              <div class="card-title mb-0 clearfix">文字訊息<div class="btn btn-icon float-right js-btn-del-card"> <i class="fa fa-trash"></i></div></div>
            </div>
            <div class="card-body">
              <div class="form-inline">
                <div class="form-group mb-3">
                  <label>Order:</label>
                  <input readonly="" class="js-inp-order form-control-plaintext w-100px" type="text" name="order_2" value="1">
                </div>
              </div>

              <textarea name="title_<?=$key?>" cols="30" rows="5" class="form-control js-inp-title mb-4" placeholder="標題"></textarea>
              <span class="form-text">* 輸入 {{last_name}} 或 {{first_name}} 即可在訊息中出現使用者的姓名</span>
              <hr>
              <!-- 若已有按鈕資料, 隱藏 "新增快速回復" Btn -->
              <div class="btn btn-main btn-block js-btn-add-content" data-type="button"><i class="fas fa-plus mr-2"></i>新增按鈕</div>
              <div class="btn btn-main btn-block js-btn-add-content" data-type="instant" style="display: none;"><i class="fas fa-plus mr-2"></i>新增快速回覆</div>
              <div class="js-added-box">

                <!-- 錯誤訊息容器 -->
                <div class="error-content-exceed-max py-2 text-danger"></div>
                <div class="js-added-content card-footer bg-light" data-type="button">
                  <span class="btn btn-icon js-btn-del-content">
                    <i class="fas fa-trash"></i>
                  </span>
                  <input type="text" class="d-inline-block mx-1" name="content_0_1" style="width:calc(100% - 10rem);" placeholder="內容">

                  <div class="dropdown clearfix d-inline-block">
                    <!-- 連結機器人模組 -->
                    <button class="btn btn-icon dropdown-toggle no-caret" type="button" id="dropdownMenuButton" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                      <i class="fa fa-random"></i>
                    </button>

                    <div class="dropdown-menu px-4 py-3 min-w-300px" aria-labelledby="dropdownMenuButton">
                      <label for="">
                        <i class="fa fa-random"></i>
                        連結機器人模組
                      </label>
                      <select class="js-selectize mb-3" name="connect_<?=$key?>_<?=$index?>" id="">
                        <option value="1">module1</option>
                        <option value="2">moduile2</option>
                        <option value="3">module3</option>
                      </select>
                      <button class="btn btn-main py-1 js-close-dropdown float-right">確定</button>
                    </div>
                  </div>

                  <div class="dropdown clearfix d-inline-block">
                    <button class="btn btn-icon dropdown-toggle no-caret" type="button" id="dropdownMenuButton" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                      <i class="fa fa-tag"></i>
                    </button>
                    <div class="dropdown-menu px-4 py-3 min-w-300px" aria-labelledby="dropdownMenuButton">
                      <label for="">
                        <i class="fa fa-tag"></i>
                        選擇標籤
                      </label>
                      <select class="js-selectize" name="tag_<?=$key?>_<?=$index?>" id="" multiple>
                        <option value="1">1</option>
                        <option value="2">2</option>
                        <option value="3">3</option>
                        <option value="4">4</option>
                        <option value="5">5</option>
                      </select>
                    </div>
                  </div>

                  <div class="dropdown clearfix d-inline-block">
                    <button class="btn btn-icon dropdown-toggle no-caret" type="button" id="dropdownMenuButton" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                      <i class="fa fa-link"></i>
                    </button>
                    <div class="dropdown-menu px-4 py-3 min-w-300px" aria-labelledby="dropdownMenuButton">
                      <label for="">
                        <i class="fa fa-link"></i>
                        外連 URL 網址
                      </label>
                    <input type="text" class="form-control" name="url_<?=$key?>_<?=$index?>" placeholder="請輸入網址">
                    </div>
                  </div>

                </div>
              </div>
            </div>
          </div>
        </div>
      </div>

      <? $key = 1 ?>
      <div class="tab-pane fade" id="tab_cont_<?=$key?>" role="tabpanel" data-type="multi-page" aria-labelledby="nav_<?=$key?>">
        <div class="js-card">
        <!-- 卡片 -->
          <div class="card mb-5">
            <div class="card-header">
              <div class="card-title mb-0 clearfix">多頁訊息<div class="btn btn-icon float-right js-btn-del-card"> <i class="fa fa-trash"></i></div></div>
            </div>
            <div class="card-body">
              <div class="form-inline">
                <div class="form-group mb-3">
                  <label>Order:</label>
                  <input readonly="" class="js-inp-order form-control-plaintext w-100px" type="text" name="order_<?=$key?>" value="1">
                </div>
              </div>

              <div class="js-subcard-wrapper">
                <? for($index = 0; $index < 2; $index++) { ?>
                <div class="card js-subcard mb-5">
                  <div class="card-header bg-light">
                    <div class="card-title mb-0 clearfix">卡片<div class="btn btn-icon float-right js-btn-del-subcard"> <i class="fa fa-trash"></i></div></div>
                  </div>

                  <div class="card-body">
                    <div class="form-group mb-3">
                      <label>卡片順序:</label>
                      <input class='js-inp-order form-control' type="text" name="order" value='<?=$index?>'>
                    </div>
                    <hr>
                    <input name="title_<?=$key?>_<?=$index?>" id="" class="form-control js-inp-title" placeholder="標題"></input>
                    <textarea name="desc_<?=$key?>_<?=$index?>" id="" cols="30" rows="3" class="form-control js-inp-desc" placeholder="簡述"></textarea>
                    <span class="form-text">* 輸入 {{last_name}} 或 {{first_name}} 即可在訊息中出現使用者的姓名</span>

                    <hr>
                    <div class="js-added-box">
                    <!-- 錯誤訊息容器 -->
                      <div class="error-content-exceed-max py-2 text-danger"></div>
                        <!-- start -->
                        <div class="js-added-content card-footer bg-light" data-type="button">
                          <input type="text" class="d-inline-block mx-1" style="width:calc(100% - 10rem);" placeholder="按鈕內容">

                          <div class="dropdown clearfix d-inline-block">
                            <!-- 連結機器人模組 -->
                            <button class="btn btn-icon dropdown-toggle no-caret" type="button" id="dropdownMenuButton" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                              <i class="fa fa-random"></i>
                            </button>
                            <div class="dropdown-menu px-4 py-3 min-w-300px" aria-labelledby="dropdownMenuButton">
                              <label for="">
                                <i class="fa fa-random"></i>
                                連結機器人模組
                              </label>
                              <select class="js-selectize mb-3" name="connect_<?=$key?>_<?=$index?>" id="">
                                <option value="1">module1</option>
                                <option value="2">moduile2</option>
                                <option value="3">module3</option>
                              </select>

                              <button class="btn btn-main py-1 js-close-dropdown float-right">確定</button>
                            </div>
                          </div>

                          <!-- 選擇多項 Tag: Selectize -->

                          <div class="dropdown clearfix d-inline-block">
                            <button class="btn btn-icon dropdown-toggle no-caret" type="button" id="dropdownMenuButton" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                              <i class="fa fa-tag"></i>
                            </button>
                            <div class="dropdown-menu px-4 py-3 min-w-300px" aria-labelledby="dropdownMenuButton">
                              <label for="">
                                <i class="fa fa-tag"></i>
                                選擇標籤
                              </label>
                              <select class="js-selectize" name="tag_<?=$key?>_<?=$index?>" id="" multiple>
                                <option value="1">1</option>
                                <option value="2">2</option>
                                <option value="3">3</option>
                                <option value="4">4</option>
                                <option value="5">5</option>
                              </select>

                            </div>
                          </div>
                          <!-- 選擇 URL -->

                          <div class="dropdown clearfix d-inline-block">
                            <button class="btn btn-icon dropdown-toggle no-caret" type="button" id="dropdownMenuButton" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                              <i class="fa fa-link"></i>
                            </button>
                            <div class="dropdown-menu px-4 py-3 min-w-300px" aria-labelledby="dropdownMenuButton">
                              <label for="">
                                <i class="fa fa-link"></i>
                                外連 URL 網址
                              </label>
                            <input type="text" name="url_<?=$key?>_<?=$index?>" class="form-control" placeholder="請輸入網址">
                            </div>
                          </div>

                      </div>
                    </div>
                  </div>
                </div>
                <? } ?>
              </div>
              <div class="btn btn-main js-btn-add-subcard"><i class="fa fa-plus mr-2"></i> 新增卡片</div>
              <div class="error-subcard-exceed-max text-danger"></div>
            </div>
          </div>
        </div>
      </div>
    </div>
  </div>
</div>
</form>

<?include('footer.php')?>
<script>
  function getTabNavTemplate ({ navId = '', type = 'text', tabContentId = '', key = 0 }) {
    var titleMap = {
      'text': '文字訊息',
      'image': '圖片訊息',
      'video': '影片訊息',
      'image-text': '圖文訊息',
      'multi-page': '多頁訊息'
    }

    return `
      <a class="js-sort-li list-group-item list-group-item-action" id="${navId}" data-toggle="list" href="#${tabContentId}" role="tab" aria-controls="${navId}">
        <i class="fas fa-arrows-alt handle"></i>
        <span class="js-num"></span>
        ${titleMap[type]}
        <div class="js-title"></div>
      </a>
      `
  }

  function getTabContentTemplate ({ navId = '', type = 'text', tabContentId = '', key = 0 }) {
    var titleMap = {
      'text': '文字訊息',
      'image': '圖片訊息',
      'video': '影片訊息',
      'image-text': '圖文訊息',
      'multi-page': '多頁訊息'
    }

    var contentMap = {
      'text': `
        <textarea name="title_${key}" cols="30" rows="5" class="form-control js-inp-title mb-4" placeholder="標題"></textarea>
        <span class="form-text">* 輸入 {{last_name}} 或 {{first_name}} 即可在訊息中出現使用者的姓名</span>
        <hr>
      `,
      'image': '<div class="upload-area"><i class="fa fa-camera"></i>圖片上傳區</div><hr>',
      'image-text': '<div class="upload-area">圖文訊息區</div><hr>',
      'multi-page': '',
      'video': '<div class="upload-area">影片上傳區</div><hr>'
    }

    var dynamicPart
    switch (type) {
      case 'multi-page':
        dynamicPart = `
          <div class="js-subcard-wrapper"></div>
          <div class="btn btn-main js-btn-add-subcard"><i class="fa fa-plus mr-2"></i> 新增卡片</div>
          <div class="error-subcard-exceed-max text-danger"></div>
        `
        break

      default:
        dynamicPart = `
          <div class="btn btn-main btn-block js-btn-add-content" data-type="button"><i class="fas fa-plus mr-2"></i>新增按鈕</div>
          <div class="btn btn-main btn-block js-btn-add-content" data-type="instant"><i class="fas fa-plus mr-2"></i>新增快速回覆</div>
          <div class="js-added-box">
          <!-- 錯誤訊息容器 -->
          <div class="error-content-exceed-max py-2 text-danger"></div>
        `
    }
    var html = `
      <div class="tab-pane fade" id="${tabContentId}" role="tabpanel" data-type="${type}" aria-labelledby="${navId}">
      <div class="js-card">
      <!-- 卡片 -->
      <div class="card mb-5">
        <div class="card-header">
          <div class="card-title mb-0 clearfix">${titleMap[type]}<div class="btn btn-icon float-right js-btn-del-card"> <i class="fa fa-trash"></i></div></div>
        </div>
        <div class="card-body">
          <div class="form-inline">
            <div class="form-group mb-3">
              <label>Order:</label>
              <input readonly class='js-inp-order form-control-plaintext w-100px' type="text" name="order_${key}" value='1'>
            </div>
          </div>
          ${contentMap[type]}
          ${dynamicPart}
        </div>
      </div>
    `

    return html
  }

  // template: 按鈕或立即回覆卡片
  function getContentTemplate ({ type = 'button', key = 0, index = 0}) {
    // type: button or instant
    var isButton = (type === 'button')

    var chooseTagTemp = `
      <div class="dropdown clearfix d-inline-block">
        <button class="btn btn-icon dropdown-toggle no-caret" type="button" id="dropdownMenuButton" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
          <i class="fa fa-tag"></i>
        </button>
        <div class="dropdown-menu px-4 py-3 min-w-300px" aria-labelledby="dropdownMenuButton">
          <label for="">
            <i class="fa fa-tag"></i>
            選擇標籤
          </label>
          <select class="js-selectize" name="tag_${key}_${index}" id="" multiple>
            <option value="1">1</option>
            <option value="2">2</option>
            <option value="3">3</option>
            <option value="4">4</option>
            <option value="5">5</option>
          </select>
        </div>
      </div>
    `
    var chooseUrlTemp = `
      <div class="dropdown clearfix d-inline-block">
        <button class="btn btn-icon dropdown-toggle no-caret" type="button" id="dropdownMenuButton" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
          <i class="fa fa-link"></i>
        </button>
        <div class="dropdown-menu px-4 py-3 min-w-300px" aria-labelledby="dropdownMenuButton">
          <label for="">
            <i class="fa fa-link"></i>
            外連 URL 網址
          </label>
        <input type="text" class="form-control" name="url_${key}_${index}" placeholder="請輸入網址">
        </div>
      </div>
    `
    var connectModuleTemp = `
      <div class="dropdown clearfix d-inline-block">
        <!-- 連結機器人模組 -->
        <button class="btn btn-icon dropdown-toggle no-caret" type="button" id="dropdownMenuButton" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
          <i class="fa fa-random"></i>
        </button>

        <div class="dropdown-menu px-4 py-3 min-w-300px" aria-labelledby="dropdownMenuButton">
          <label for="">
            <i class="fa fa-random"></i>
            連結機器人模組
          </label>
          <select class="js-selectize mb-3" name="connect_${key}_${index}" id="">
            <option value="1">module1</option>
            <option value="2">moduile2</option>
            <option value="3">module3</option>
          </select>

          <button class="btn btn-main py-1 js-close-dropdown float-right">確定</button>
        </div>
      </div>
    `

    var html =  `
      <div class="js-added-content card-footer bg-light" data-type="${type}">
        <span class="btn btn-icon js-btn-del-content">
          <i class="fas fa-trash"></i>
        </span>
        <input type="text" class="d-inline-block mx-1" name="content_${key}_${index}" style="width:calc(100% - 10rem);" placeholder="內容">
        ${connectModuleTemp}
        ${chooseTagTemp}
        ${isButton ? chooseUrlTemp : ''}
      </div>
      `
      return html
  }

  // 多頁訊息
  function getSubCard ({key = 0, index = 0}) {
    var html = `
      <div class="card js-subcard mb-5">
        <div class="card-header bg-light">
          <div class="card-title mb-0 clearfix">卡片<div class="btn btn-icon float-right js-btn-del-subcard"> <i class="fa fa-trash"></i></div></div>
        </div>
        <div class="card-body">
          <div class="form-group mb-3">
            <label>卡片順序:</label>
            <input class='js-inp-order form-control' type="text" name="order_${key}_${index}" value='${index}'>
          </div>
          <hr>
          <input name="title_${key}_${index}" id="" class="form-control js-inp-title" placeholder="標題"></input>
          <textarea name="desc_${key}_${index}" id="" cols="30" rows="3" class="form-control js-inp-desc" placeholder="簡述"></textarea>
          <span class="form-text">* 輸入 {{last_name}} 或 {{first_name}} 即可在訊息中出現使用者的姓名</span>

        <hr>
        <div class="js-added-box">
        <!-- 錯誤訊息容器 -->
          <div class="error-content-exceed-max py-2 text-danger"></div>
          <div class="js-added-content card-footer bg-light">
            <!-- start -->
                  <input type="text" class="d-inline-block mx-1" style="width:calc(100% - 10rem);" placeholder="按鈕內容">

                  <div class="dropdown clearfix d-inline-block">
                    <!-- 連結機器人模組 -->
                    <button class="btn btn-icon dropdown-toggle no-caret" type="button" id="dropdownMenuButton" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                      <i class="fa fa-random"></i>
                    </button>
                    <div class="dropdown-menu px-4 py-3 min-w-300px" aria-labelledby="dropdownMenuButton">
                      <label for="">
                        <i class="fa fa-random"></i>
                        連結機器人模組
                      </label>
                      <select class="js-selectize mb-3" name="connect_${key}_${index}" id="">
                        <option value="1">module1</option>
                        <option value="2">moduile2</option>
                        <option value="3">module3</option>
                      </select>

                      <button class="btn btn-main py-1 js-close-dropdown float-right">確定</button>
                    </div>
                  </div>

                  <!-- 選擇多項 Tag: Selectize -->

                  <div class="dropdown clearfix d-inline-block">
                    <button class="btn btn-icon dropdown-toggle no-caret" type="button" id="dropdownMenuButton" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                      <i class="fa fa-tag"></i>
                    </button>
                    <div class="dropdown-menu px-4 py-3 min-w-300px" aria-labelledby="dropdownMenuButton">
                      <label for="">
                        <i class="fa fa-tag"></i>
                        選擇標籤
                      </label>
                      <select class="js-selectize" name="tag_${key}_${index}" id="" multiple>
                        <option value="1">1</option>
                        <option value="2">2</option>
                        <option value="3">3</option>
                        <option value="4">4</option>
                        <option value="5">5</option>
                      </select>

                    </div>
                  </div>
                  <!-- 選擇 URL -->

                  <div class="dropdown clearfix d-inline-block">
                    <button class="btn btn-icon dropdown-toggle no-caret" type="button" id="dropdownMenuButton" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                      <i class="fa fa-link"></i>
                    </button>
                    <div class="dropdown-menu px-4 py-3 min-w-300px" aria-labelledby="dropdownMenuButton">
                      <label for="">
                        <i class="fa fa-link"></i>
                        外連 URL 網址
                      </label>
                    <input type="text" class="form-control" name="url_${key}_${index}" placeholder="請輸入網址">
                    </div>
                  </div>
              <!-- end -->
          </div>
        </div>

        </div>
      </div>
    `
    return html
  }
</script>

<script>
  // var idxMap = {
  //   1: { type: 'text', lastIndex: 3 },
  //   3: { type: 'text', lastIndex: 3 },
  //   4: { type: 'text', lastIndex: 3 },
  //   5: { type: 'image', lastIndex: 0 }
  // }

  function getLastKeyInMap (map) {
    return Array.from(map.keys()).pop();
  }

// Init : 表單 input 編號
  function initIdxMap () {
    var map = new Map()

    var tabNavWrap = $('#tabList_wrap')
    var tabContWrap = $('#tabContent_wrap')

    tabContWrap.children('.tab-pane').each(function (key, el) {
      var type = $(el).attr('data-type')
      var lastIndex, dynamicBox

      switch (type) {
        case 'multi-page':
          lastIndex = $(el).find('.js-subcard').length

          // bind btn add subcard
          $(el).find('.js-btn-add-subcard').on('click', function (e) {
            var _this = $(e.target)
            var dynamicBox = $(el).find('.js-subcard-wrapper')
            var errorBox = $(el).find('.error-subcard-exceed-max')

            var newLastIdx = idxMap.get(key).lastIndex + 1
            var curLength = $(el).find('.js-added-content').length

            if (dynamicBox.children().length >= 10) {
              errorBox.text('卡片不可超過 10 個')
            } else {
              dynamicBox.append(getSubCard({ key, index: newLastIdx }))

              bindBtnDelSubCard()
              setupSelectize()

              // update index map while adding subcard
              map.set(key, { type, lastIndex: newLastIdx })
            }
          })

          break

        default:
          lastIndex = $(el).find('.js-added-content').length

          // bind btn add content
          $(el).find('.js-btn-add-content').on('click', function (e) {
            var _this = $(e.target)
            var errorBox = $(el).find('.error-content-exceed-max')
            var dynamicBox = $(el).find('.js-added-box')

            var contentType = _this.attr('data-type')
            var newLastIdx = idxMap.get(key).lastIndex + 1
            var curLength = $(el).find('.js-added-content').length

            var max = 3

            // btn add content
            if (curLength === 0) {
              _this.siblings(`.js-btn-add-content[data-type!=${contentType}]`).hide()
            }
            if (curLength >= max) {
              errorBox.text(`按鈕或快速回覆不可超過${max}個`)
            } else {
              errorBox.text('')
              dynamicBox.append(getContentTemplate({ type: contentType, key, index: newLastIdx }))

              bindBtnDelContent()
              setupSelectize()

              // update index map while adding content
              map.set(key, { type, lastIndex: newLastIdx })
            }
          })

          break
        }
        // init index map
        map.set(key, { type, lastIndex })
    })

    return map
  }

  function bindBtnAddCard () {
    // 新增訊息卡
    $('.js-btn-add-card').on('click', function (e) {
      var _this = $(e.target)
      var tabNavWrap = $('#tabList_wrap')
      var tabContWrap = $('#tabContent_wrap')
      var errorWrap = $('#err_list_exceed_max_len')

      var type = _this.attr('data-type')
      var curLength = $(tabNavWrap).children('.list-group-item').length

      if (curLength >= 10) {
        errorWrap.text('訊息卡片數量最多十個')
      } else {
        errorWrap.text('')

        var newKey = getLastKeyInMap(idxMap) + 1

        var navId = 'nav_' + newKey
        var tabContentId = 'tab_cont_' + newKey

        var navHtml = getTabNavTemplate({
          navId: navId,
          type: type,
          tabContentId: tabContentId,
          key: newKey
        })

        var tabContHtml = getTabContentTemplate({
          navId: navId,
          type: type,
          tabContentId: tabContentId,
          key: newKey
        })

        tabNavWrap.append(navHtml)
        tabContWrap.append(tabContHtml)

        var el = document.getElementById(tabContentId)
        var lastIndex, dynamicBox

        switch (type) {
          case 'multi-page':
            lastIndex = $(el).find('.js-subcard').length

            idxMap.set(newKey, { type, lastIndex })

            // Bind Btn add subcard
            $(el).find('.js-btn-add-subcard').on('click', function (e) {
              var _this = $(e.target)
              var dynamicBox = $(el).find('.js-subcard-wrapper')
              var errorBox = $(el).find('.error-subcard-exceed-max')

              var newLastIdx = idxMap.get(newKey).lastIndex + 1

              if (dynamicBox.children().length >= 10) {
                errorBox.text('卡片不可超過 10 個')
              } else {
                dynamicBox.append(getSubCard({ key: newKey, index: newLastIdx }))

                idxMap.set(newKey, { type, lastIndex: newLastIdx })

                bindBtnDelSubCard()
                setupSelectize()
              }
            })
            break

          default:
            lastIndex = $(el).find('.js-added-content').length

            idxMap.set(newKey, { type, lastIndex })

            // Bind Btn add content
            $(el).find('.js-btn-add-content').on('click', function (e) {
              var _this = $(e.target)
              var dynamicBox = $(el).find('.js-added-box')
              var errorBox = $(el).find('.error-content-exceed-max')

              var contentType = _this.attr('data-type')
              var curLength = $(el).find('.js-added-content').length
              var newLastIdx = idxMap.get(newKey).lastIndex + 1

              var max = 3

              if (curLength === 0) {
                _this.siblings(`.js-btn-add-content[data-type!=${contentType}]`).hide()
              }

              if (curLength >= max) {
                errorBox.text(`按鈕或快速回覆不可超過${max}個`)
              } else {
                errorBox.text('')
                // 新增按鈕或快速回覆
                dynamicBox.append(getContentTemplate({ type: contentType, key: newKey, index: newLastIdx }))

                idxMap.set(newKey, { type, lastIndex: newLastIdx })

                bindBtnDelContent()
                setupSelectize()
              }
            })

            break
        }

        bindBtnDelCard ()
        updateOrderNum ()
      }
    })

    // setUpSortable()

  }

  function bindBtnDelCard () {
    $('.js-btn-del-card').on('click', function (e) {
      var _this = $(e.target)
      var tabContent = _this.closest('.tab-pane')
      var tabNavId = '#' + tabContent.attr('aria-labelledby')

      // remove nav-list-item
      $(tabNavId).remove()

      // remove card
      var delTarget = _this.closest('.tab-pane').remove()

      $('#err_list_exceed_max_len').text('')
    })
  }

  function setUpSortable () {
    var el = document.getElementById('tabList_wrap')

    new Sortable(el, {
      handle: '.js-sort-li',
      // handle: '.handle', // handle's class
      animation: 150,
      onUpdate: function (e) {
        updateOrderNum()
      },
    });

  }

  // $('.js-inp-title').on('change', function (e) {
  //   var $this = $(this)
  //   var text = $this.val()
    
  //   var navId = '#' +  $this.closest('.tab-pane').attr('aria-labelledby')
    
  //   $(navId).find('.js-title').text(text)
  // })

  function updateOrderNum (selector = '.js-sort-li') {
    $(selector).each(function (idx, el) {
      var tarId = $(el).attr('href');
      var orderNum = idx + 1
      
      setTimeout(
        () => {
          $(el).find('.js-num').html(orderNum)
          $(tarId).find("input.js-inp-order").val(orderNum)
        }
        , 200)
      
    
    })
  }

  var idxMap = initIdxMap()

  setupDropdown()
  setUpSortable()

  bindBtnAddCard ()
  bindBtnDelCard ()
  // bindBtnAddContent()
  bindBtnDelContent()
  bindBtnDelSubCard()

  setupSelectize()
  updateOrderNum()

  function setupDropdown () {
    $(document).on('click', '.js-card .dropdown-menu', function (e) {
      e.stopPropagation();
    });
  }

  function bindBtnDelContent () {
    $('.js-btn-del-content').on('click', function (e) {
      _this = $(e.target)
      var wrapper = _this.closest('.js-added-box')
      var delTarget = _this.closest('.js-added-content')

      delTarget.remove()

      var curLength = wrapper.children('.js-added-content').length
      if (curLength === 0) {
        wrapper.siblings('.js-btn-add-content').show()
      }

      wrapper.children('.error-content-exceed-max').text('')
    })
  }

    // 多頁訊息的卡片
    

  function bindBtnDelSubCard () {
    $('.js-btn-del-subcard').on('click', function (e) {
      var _this = $(e.target)
      var delTarget = _this.closest('.js-subcard')
      var wrapper = _this.closest('.js-subcard-wrapper')

      delTarget.remove()

      wrapper.siblings('.error-subcard-exceed-max').text('')
    })
  }

  function setupSelectize () {
      $('.js-selectize').selectize();
  }
</script>